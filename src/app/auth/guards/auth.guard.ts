import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { UserService } from "../services/user.service";
import { Observable } from "rxjs";
import { ErrorService } from "../../error/error.service";
import APP_LOCATIONS from './auth.guard.locations.json';

export declare interface LocationPermissionAccount {
  name: string;
}

export declare interface LocationPermissionTarget {
  name?: string;
  url: string;
  pattern?: RegExp;
}

export declare interface LocationPermission {
  privilege: string;
  account?: LocationPermissionAccount;
  target: LocationPermissionTarget;
  mask?: number;
}

@Injectable()
export class AuthGuard implements CanActivate {

  private _locations: any = APP_LOCATIONS;


  constructor(private _router: Router,
    private _errorService: ErrorService,
    private _userService: UserService) {
    this._locations.forEach((x) => {
      if (typeof x.target.pattern === 'undefined' || x.target.pattern === null) {
        x.target.pattern = new RegExp(x.target.url, 'i');
      }
    });
  }

  public canActivateLocation(path: string, user: any): LocationPermission {
    let groups = [];
    if (user && user.groups) {
      groups = user.groups.map((x) => {
        return x.name;
      });
    }
    return this._locations.find((x) => {
      return x.target.pattern.test(path)
        && (typeof x.account === 'undefined' || groups.indexOf(x.account.name) >= 0)
        && (x.mask === 0 || ((x.mask & 1) === 1))
        && user;
    });

  }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return new Observable<boolean>(resolve => {
      this._userService.getUser().subscribe((res) => {
        let location = this.canActivateLocation(state.url, res);
        if (location && (typeof location.account === 'undefined')) {
          return resolve.next(true);
        }
        if (res) {
          if (location && (location.mask & 1) === 1) {
            return resolve.next(true);
          }
          else {
            // noinspection JSIgnoredPromiseFromCall
            this._router.navigate(['/error/403.1'], {
              queryParams: {
                action: 'LoginAsDifferentUser',
                continue: '/auth/loginAs'
              }
            });
            return resolve.next(false);
          }
        }
        // noinspection JSIgnoredPromiseFromCall
        this._router.navigate(['/auth/login']);
        return resolve.next(false);
      });
    });
  }
}
