import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ProfileService } from '../../profile/services/profile.service';

@Injectable()

export class CurrentRegistrationService {
    // Current registration, including all the courses to be registered
    private currentRegistration: any = {};

    private ects;

    constructor(private _context: AngularDataContext, private _profileService: ProfileService) {

    }

    getLastRegistration() {
        return this._context.model('students/me/registrations')
            .asQueryable()
            .orderByDescending('academicYear')
            .thenByDescending('academicPeriod')
            .getItem();
    }

    getCurrentRegistrationEffectiveStatus() {
        return this._context.model('students/me/currentRegistration/effectiveStatus')
            .asQueryable()
            .getItem();
    }

    getAvailableClasses() {
        return this._context.model('students/me/availableClasses')
                    .asQueryable()
                    .take(-1)
                    .getItems();
    }

    getStudyLevel() {
        return this._context.model('students/me/studyProgram')
                    .select('studyLevel')
                    .getItem();
    }

    // To get the current registration we have 2 options during a session:
    // If user registered for some courses during the CURRENT session
    // then the current registration will be saved in session storage.
    // Else, we get the current registration by the API call.
    getCurrentRegistration(forceNew?: boolean) {
        if (sessionStorage['RegistrationLatest']) {
            return new Promise( (resolve, reject) => {
                resolve(JSON.parse(sessionStorage['RegistrationLatest']));
            });
        } else {
        return this._context.model('students/me/currentRegistration')
                    .asQueryable()
                    .expand('classes($expand=courseClass($expand=instructors($expand=instructor\
                        ($select=id,givenName,familyName,category))),courseType)')
                    .getItem().then(currentRegistration => {
                        // save the initial registraiton to session storage
                        // in order to track changes and show/hide the submit button
                        sessionStorage['InitialRegistration'] = JSON.stringify(currentRegistration.classes);
                        return currentRegistration;
                    }).catch(err => {
                        if (err.status === 404 && forceNew) {
                            return this._profileService.getStudent().then(student => {
                                if (student.studentStatus.alternateName !== 'active'
                                && student.studentStatus.alternateName !== 'candidate') {
                                    return Promise.reject(err);
                                }
                                // create new registration for current year period
                                const newRegistration = {
                                    student: student.id,
                                    registrationYear: student.department.currentYear,
                                    registrationPeriod: student.department.currentPeriod,
                                    classes: []
                                };
                                // set newRegistration to session storage variable
                                sessionStorage.setItem('RegistrationLatest', JSON.stringify(newRegistration));
                                sessionStorage.setItem('InitialRegistration', JSON.stringify(newRegistration.classes));
                                return Promise.resolve(newRegistration);
                            });
                        }
                        return Promise.reject(err);
                    });
        }
    }

    saveCurrentRegistration() {
        return this.getCurrentRegistration().then(currentRegistration => {
            return this._context.model('students/me/currentRegistration')
                .save(currentRegistration);
        });
    }

    // function that registers for the clicked course
    registerForCourse(course: any) {
        // Save the renewed current registration to session storage
        return this.getCurrentRegistration().then(currentReg => {
            if (currentReg.classes) {
                // push the new registered course to the current Reigstration array
                currentReg.classes.push(course);
            } else {
                Object.assign(currentReg, {classes: []});
                currentReg.classes.push(course);
            }
            // convert it into string in order to save it to session storage
            const tempString = JSON.stringify(currentReg);

            // save to session storage
            sessionStorage.setItem('RegistrationLatest', tempString);

            return 0;
        });
    }

    removeCourse(course) {
        // remove from current registration
        return this.getCurrentRegistration().then(currentReg => {
            // find course index
            for (let i = 0; i < currentReg.classes.length; ++i) {
                if (currentReg.classes[i].courseClass.id === course.courseClass
                    || currentReg.classes[i].courseClass === course.courseClass) {
                    // remove course from current registration
                    currentReg.classes.splice(i, 1);
                    break;
                }
            }
            // convert current registration into string in order to save it to session storage
            const tempString = JSON.stringify(currentReg);

            // save to session storage
            sessionStorage.setItem('RegistrationLatest', tempString);

            return 0;
        });
    }

    getStudentStatus() {
        return this._context.model('students/me/studentStatus')
                    .asQueryable()
                    .getItems();
    }

    reset() {
        sessionStorage.removeItem('RegistrationLatest');
        sessionStorage.removeItem('InitialRegistration');
        sessionStorage.removeItem('edit');
        sessionStorage.removeItem('edit');
    }

    registerSemester() {
        this._profileService.getStudent().then(student => {
            // get student department current year and period
            return this._context.model('students/me/currentRegistration')
                .save({
                    'student': student.id,
                    'year': student.department.currentYear,
                    'period': student.department.currentPeriod
                });
        });
    }

    getRegistrationStatus(): Promise<any> {
        const self = this;
        // get student
        return new Promise<number> ((resolv, reject) => {
            this._profileService.getStudent().then(student => {
                // get current registration
                this.getCurrentRegistrationEffectiveStatus().then(currentStatus => {
                    this.getStudentStatus().then(studentStatus => {
                        if (!student) {
                            return reject(new Error('Current student cannot be found'));
                        }
                        if ( !studentStatus || !currentStatus ) {
                            // Undefined studentStatus || currentStatus
                            return reject(0);
                        }
                        if ( studentStatus.alternateName !== 'active'
                            && studentStatus.alternateName !== 'candidate') {return resolv(6); }

                        // Undergraduate
                        if (student.studyProgram.studyLevel === 1) {
                            if (currentStatus.status === 'open') {
                                if (student.department.isRegistrationPeriod) {
                                    // Available
                                    if (currentStatus.code === 'OPEN_NO_TRANSACTION') {
                                        return resolv(2);
                                    } else {
                                        // Already Registered
                                        return resolv(1);
                                    }
                                }
                            } else {
                                // Deadlined passed
                                if (currentStatus.code === 'CLOSED_NO_REGISTRATION'
                                    || currentStatus.code === 'CLOSED_REGISTRATION_PERIOD') {
                                    return resolv(4);
                                } else {
                                    // NotAvailableTill
                                    return resolv(5);
                                }
                            }
                        // Postgraduate
                        } else if (student.studyProgram.studyLevel === 2) {
                            return resolv(3);
                        }
                    });
                });
            });
        });
    }
}
