import { Component } from '@angular/core';

@Component({
    selector: 'registrations-home',
    template: `
      <div class="clear-content-area">
        <div class="d-none d-md-block">
          <ul class="nav nav-tabs">
              <li class="nav-item">
                  <a class="nav-link" [routerLink]="['semester']" routerLinkActive="active" 
                    [translate]="'Registrations.RegistrationSemester'" ></a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" [routerLink]="['courses']" routerLinkActive="active" 
                    [translate]="'Registrations.RegistrationCourses'" ></a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" [routerLink]="['list']" routerLinkActive="active"
                    [translate]="'Registrations.RegistrationList'"></a>
              </li>
          </ul>
        </div>
            <router-outlet></router-outlet>
      </div>
    `
})

export class RegistrationsHomeComponent {

}