import { Component, AfterViewInit, ElementRef, Renderer2, OnInit } from '@angular/core';

@Component({
  selector: 'app-spinner',
  template: `
  <div [ngStyle]="{'text-align': 'center',
  'display': 'block'}">
  <sk-three-bounce></sk-three-bounce>
  </div>`
})
export class SpinnerComponent implements OnInit {

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
    this.renderer.setStyle(this.el.nativeElement.parentElement, 'background-color', 'transparent');
    this.renderer.setStyle(this.el.nativeElement.parentElement, 'border', '0px');
  }
}
