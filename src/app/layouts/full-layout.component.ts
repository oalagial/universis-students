import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html',
  styleUrls: ['full-layout.component.scss']
  })
export class FullLayoutComponent implements OnInit {

  constructor(private context:AngularDataContext) {
  }

  public disabled = false;
  public status: { isopen: boolean } = { isopen: false };
  public myName: string = '';

  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  ngOnInit(): void {
    this.context.model('students/me')
      .asQueryable()
        .select('person/givenName as givenName, person/familyName as familyName')
      .getItems().then((res) => {
        this.myName = res.givenName.charAt() + "." + res.familyName.charAt() + ".";
      });
  }
}
