import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { GradesRoutingModule } from './grades-routing.module';
import { GradesDashboardComponent } from './components/grades-dashboard/grades-dashboard.component';
import {environment} from '../../environments/environment';
import { GradesRecentComponent } from './components/grades-recent/grades-recent.component';
import { GradesAllComponent } from './components/grades-all/grades-all.component';
import {GradesHomeComponent} from "./components/grades-home/grades-home.component";
import { GradesStatboxComponent } from './components/grades-statbox/grades-statbox.component';
import {CollapseModule} from 'ngx-bootstrap';
import { GradesProjectComponent } from './components/grades-project/grades-project.component';
import {FormsModule} from "@angular/forms";
import {NgPipesModule} from "ngx-pipes";
import {GradesSharedModule} from "./grades-shared.module";

@NgModule({
  imports: [
      FormsModule,
    CommonModule,
      NgPipesModule,
    GradesRoutingModule,
    TranslateModule,
    CollapseModule.forRoot(),
      GradesSharedModule
  ],
  declarations: [
    GradesDashboardComponent,
    GradesRecentComponent,
    GradesAllComponent,
    GradesHomeComponent,
    GradesStatboxComponent,
    GradesProjectComponent
  ],
  exports: [
    GradesDashboardComponent
  ]
})
export class GradesModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      import(`./i18n/grades.${culture}.json`).then((translations) => {
        this._translateService.setTranslation(culture, translations, true);
      });
    });
  }
}
