import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GradesRecentComponent } from './grades-recent.component';

describe('GradesRecentComponent', () => {
  let component: GradesRecentComponent;
  let fixture: ComponentFixture<GradesRecentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GradesRecentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradesRecentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
